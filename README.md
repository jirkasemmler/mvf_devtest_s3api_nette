MVF Test
========

Installation
------------
 1) clone repository
 2) composer install
 3) create and set aws credentials in app/config/config.local.neon. the config.neon is default but the local one must be created
 4) create directories temp and log in the root

Implemented features
--------------------
 1) Customer Accounts - /api/v1/customer/<customer guid>/accounts
 2) Account - /api/v1/account/<account guid>
 3) Account field -/api/v1/account/<account guid>/<field>
 4) Authentication

Usage
-----
Send http GET requests to noticed endpoints with Basic authentication
demo on http://mvf.jirisemmler.eu/

Technologies
------------
1) Framework - https://nette.org/en/
2) REST extension - https://github.com/drahak/Restful 
3) AWS/SDK - https://aws.amazon.com/sdk-for-php/

My code
-------
app/RestModule
~14hours
