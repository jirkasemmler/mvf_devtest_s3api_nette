<?php
/**
 * Created by PhpStorm.
 * User: JiriSemmler
 * Date: 2.9.17
 * Time: 17:11
 */

namespace App\Presenters;

use Nette;
use Nette\Application\Responses;
use Tracy\ILogger;

class ErrorPresenter implements Nette\Application\IPresenter
{
    use Nette\SmartObject;

    /** @var ILogger */
    private $logger;


    public function __construct(ILogger $logger)
    {
        $this->logger = $logger;
    }

    /**
     * custom handler for exceptions
     * @param Nette\Application\Request $request
     * @return Responses\JsonResponse
     */
    public function run(Nette\Application\Request $request)
    {
        $exception = $request->getParameter('exception');

        if ($exception instanceof Nette\Application\ForbiddenRequestException) {
            return new Responses\JsonResponse(['error' => $exception->getMessage()]);
        } else {
            return new Responses\JsonResponse(['error' => $exception->getMessage()]);
        }
    }
}
