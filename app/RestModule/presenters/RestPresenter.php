<?php
/**
 * Created by PhpStorm.
 * User: JiriSemmler
 * Date: 2.9.17
 * Time: 16:48
 */

namespace App\RestModule\Presenters;

use Drahak\Restful\IResource;
use Drahak\Restful\Application\UI\SecuredResourcePresenter;
use Auth;
use App\RestModule\Model\MvfAws;
use Nette\Neon\Exception;

class RestPresenter extends SecuredResourcePresenter
{
    protected $typeMap = array(
        'json' => IResource::JSON,
        'xml' => IResource::XML
    );

    private $type;
    /**
     * @var Auth
     */
    public $auth;
    /**
     * @var MvfAws
     */
    protected $mvfAws;

    public function __construct(MvfAws $mvfAws, Auth $auth) {
        parent::__construct();
        $this->mvfAws = $mvfAws; // classes by DI
        $this->auth = $auth;
    }

    protected function startup() {
        parent::startup();

        $this->type = $this->typeMap['json']; //setting the type of response
        $this->authentication->setAuthProcess($this->auth);  // setting custom validator
    }

    /**
     * sending list of customer's accounts
     * @param $customerGuid
     */
    public function actionCustomer($customerGuid) {
        $accounts = $this->mvfAws->getCustomersAccounts($customerGuid);
        $accountsIDs = array_map(function ($x) {
            return $x->id;
        }, (array) $accounts);
        $this->resource = $accountsIDs;
        $this->send();
    }

    /**
     * returns data about account
     * @GET
     * @param $accountGuid
     * @param null $field
     * @throws Exception
     */
    public function actionAccount($accountGuid, $field = null) {
        $account = $this->mvfAws->getAccount($accountGuid);
        if ($field) {
            if (property_exists($account, $field)) {
                $this->resource->$field = $account->$field;
            }
            else {
                throw new Exception("undefined variable " . $field);
            }
        }
        else {
            $this->resource = (array)$account;
        }
        $this->send();
    }

    /**
     * sending the payload
     */
    public function send() {
        parent::sendResource($this->type);
    }
}
