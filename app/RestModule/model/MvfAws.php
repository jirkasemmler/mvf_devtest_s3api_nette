<?php
/**
 * Created by PhpStorm.
 * User: JiriSemmler
 * Date: 3.9.17
 * Time: 14:05
 */

namespace App\RestModule\Model;

use App\RestModule\Model\Aws\Aws;
use Nette\Application\BadRequestException;
use Nette\Neon\Exception;


class MvfAws
{
    /**
     * @var Aws
     */
    protected $aws; //AWS object for communication with S3 AWS


    public function __construct(Aws $aws) // DI sets the Aws object
    {
        $this->aws = $aws;
    }

    /**
     * returns list accounts for selected customer
     * @param $customerGuid string - GUID of the requested customer
     * @param bool $hasSuffix
     * @return object
     * @throws Exception - exception if customer is not found
     */
    public function getCustomersAccounts($customerGuid, $hasSuffix = false) {
        //customerGuid is name of the json file for the accounts. the caller can have it just as a GUID with the JSON suffix
        $path = ($hasSuffix) ? $customerGuid : $customerGuid . ".json";
        $data = $this->aws->getObject($path); // receiving data from AWS
        $decoded = json_decode($data['Body']);  //decoding string data from Body
        if ($decoded and property_exists($decoded, 'accounts')) {
            return $decoded->accounts; // the Body has accounts -> returning
        }
        else {
            throw new Exception("invalid-data");
        }
    }

    /**
     * returns info about selected account
     * @param $accountGuid string - identification of account
     * @return array
     * @throws BadRequestException
     */
    public function getAccount($accountGuid) {
        $users = $this->aws->listBucket(); // all possible users
        foreach ($users as $userFile) {
            try {
                $accounts = $this->getCustomersAccounts($userFile, true); // all accounts for this customer
                $accountsIDs = array_map(function ($item) {
                    return $item->id;
                }, (array)$accounts);
                //GUIDs of all accounts

                //searching for the account
                if (in_array($accountGuid, $accountsIDs)) {
                    return array_filter((array)$accounts, function ($item) use ($accountGuid) {
                        return ($item->id == $accountGuid);
                    })[0];
                }
            }
            catch (Exception $e) {  // the exception is not error here
            };
        }
        throw new BadRequestException("resource-not-found");  // nothing found
    }
}