<?php
/**
 * Created by PhpStorm.
 * User: JiriSemmler
 * Date: 2.9.17
 * Time: 18:41
 */

namespace App\RestModule\Model\Aws;

use Aws\Sdk;

use Aws\S3\Exception\S3Exception;
use Nette\Application\BadRequestException;
use Nette\Application\ForbiddenRequestException;
use Nette\DI\Container;

/**
 * main communication with AWS
 * Class Aws
 * @package App\RestModule\Model\Aws
 */
class Aws
{
    /**
     * @var array
     */
    protected $params;
    /**
     * @var array
     */
    protected $config;
    /**
     * @var \Aws\S3\S3Client
     */
    protected $client;
    /**
     * @var array
     */
    protected $requestConfig;
    /**
     * @var Sdk
     */
    protected $sdk;

    public function __construct(Container $container)
    {
        // params contain data from neon config
        $this->params = $container->parameters;

        // credentials for bucket
        $credentials = ["key" => $this->params['aws']['key'],
            "secret" => $this->params['aws']['secret']];

        //configuration for communication with AWS
        $this->config = ['region' => $this->params['aws']['region'],
            'version' => $this->params['aws']['version'],
            'credentials' => $credentials
        ];

        // request config must contain the bucket name for each touch
        $this->requestConfig = ['Bucket' => $this->params['aws']['bucketName']];
        $this->createClient();
    }

    protected function createClient()
    {
        $this->sdk = new Sdk($this->config);
        $this->client = $this->sdk->createS3();
    }

    private function getConfig(array $parameters = [])
    {
        return array_merge($this->requestConfig, $parameters);
    }

    public function listBucket($keys = ['Key'], $flat = true)
    {
        $out = [];
        $res = $this->client->listObjects($this->getConfig())->get('Contents');
        if($flat and count($keys))
        {
            $key = $keys[0];
            foreach ($res as $item)
            {
                $out[] = $item[$key];
            }
        }
        else
        {
            $out = array_map(function ($item) use ($keys) {
                $out = [];
                foreach ($keys as $key)
                {
                    $out[$key] = $item[$key];
                }
                return $out;
            }, $res);
        }
        return $out;
    }

    public function getObject($path)
    {
        try
        {
            return $this->client->getObject($this->getConfig(['Key'=>$path]));
//            $this->client->get
        }
        catch (S3Exception $e)
        {
            if($e->getStatusCode() == 403)
            {
                throw new ForbiddenRequestException("forbidden");
            }
            else
            {
                throw new BadRequestException("resource-not-found");
            }
        }
    }

}
