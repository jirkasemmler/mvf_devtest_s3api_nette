<?php

namespace App;

use Nette;
use Nette\Application\Routers\RouteList;
use Drahak\Restful\Application\Routes\ResourceRoute;
use Drahak\Restful\Application\IResourceRouter;

class RouterFactory
{
	use Nette\StaticClass;

	/**
	 * @return Nette\Application\IRouter
	 */
	public static function createRouter()
	{
		$router = new RouteList;
        $router[] = self::createRestRoutes();
		return $router;
	}

	public static function createRestRoutes()
    {
        /*
        /api/v1/customer/<customer guid>/accounts
            // List the account ids (guid) of all accounts for the customer
        /api/v1/account/<account guid>
            Return all the details of the account specified
        /api/v1/account/<account guid>/<field>
            Return the field requested for the account specified
        ----
        /api/v1/customer/<customer guid>/accounts/?lastname=Smith
        /api/v1/customer/<customer guid>/accounts/?<design the query required>

        /api/v1/customer/<customer guid>/accounts/?<design the query required>
        /api/v1/customer/<customer guid>/accounts/?<design the query required>

        /api/v1/customer/<customer guid>/accounts/?<design the query required>
        /api/v1/customer/<customer guid>/accounts/?<any query>&<sort by>
        */

    $api_prefix = "api/v1";
    $router = new RouteList('Rest');


        $router[] = new ResourceRoute($api_prefix.'/customer/<customerGuid>/accounts', array(
            'presenter' => 'Rest',
            'action' => array(
                IResourceRouter::GET => 'customer',
            )
        ), IResourceRouter::GET);


        $router[] = new ResourceRoute($api_prefix.'/account/<accountGuid>[/<field>]', array(
            'presenter' => 'Rest',
            'action' => array(
                IResourceRouter::GET => 'account',
            )
        ), IResourceRouter::GET);


        return $router;
    }
}
